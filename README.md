Space Pictures
 
 Overview
-------------
This data from https://api.nasa.gov/api.html#MarsPhotos

It can
1) View api results
2) Login into the system
3) Register into the system
4) Add to favourites from api results

Code Dependencies
-------
None

Versions
-------
* php 7.4
* mysql 5.7
* Symfony 5.2.6


Installation
-------
1. Initialize git
```bash
git init
```
2. Set git path
```bash
git remote add origin https://gitlab.com/4hasitha/symfony_pictures.git
```
3. Fetch branch
```bash
git fetch origin development
```
4. Checkout to the branch
```bash
git checkout development
```
5. Install vendor
```bash
composer install
```
6. Create database (refer .env mysql section)
```bash
php bin/console doctrine:database:create
```

7. Create migrations
```bash
php bin/console make:migration
```
8. Run Migrations
```bash
php bin/console doctrine:migrations:migrate
```
9. Run updates
```bash
php bin/console doctrine:schema:update --force
```
10. Start vertual server
```bash
symfony server:ca:install
symfony server:start
```
