<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Favourite;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(): Response
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(is_object($user)){
            $em = $this->getDoctrine()->getManager();
            $favourites = $em->getRepository('App\Entity\Favourite')
                ->findBy(array('user_id' => $user->getId() ));
            $url = json_decode(file_get_contents("https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=10&api_key=3hVk85ZVP60SeT0B92FcCzDiOPoI3iBvzM5Td8KT"), true);
            return $this->render('home/index.html.twig', [
                'controller_name' => 'HomeController',
                'url' => $url,
                'favourites' => $favourites,
            ]);
        }else{
            return $this->redirect('/login');
        }
    }

    /**
     * @Route("/add_favorite", name="add_favorite")
     */
    public function addFavorite(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(is_object($user)){
            $content = file_get_contents($request->query->get('image'));

            //Store in the filesystem.
            $fileName = "images/earth/"."image".date("ymdHis").rand(1000,9999).".png";
            $fp = fopen($fileName, "w");
            fwrite($fp, $content);
            fclose($fp);

            $entityManager = $this->getDoctrine()->getManager();

            $em = $this->getDoctrine()->getManager();
            $favourites = $em->getRepository('App\Entity\Favourite')
                ->findBy(array('user_id' => $user->getId() ));

            $isAlreadyInFavouriteList = "false";
            foreach($favourites as $favourite){
                if($favourite->getApiId() == $request->query->get('id')){
                    $isAlreadyInFavouriteList = "true";
                }
            }

            if($isAlreadyInFavouriteList == "false"){
                $favourite = new Favourite();
                $favourite->setUserId($user->getId());
                $favourite->setRoverName($request->query->get('rover_name'));
                $favourite->setCameraName($request->query->get('camera_name'));
                $favourite->setEarthDate($request->query->get('earth_date'));
                $favourite->setImage($fileName);
                $favourite->setApiId(intval($request->query->get('id')));
                $favourite->setApiImageSrc($request->query->get('image'));

                $entityManager->persist($favourite);
                $entityManager->flush();
            }

            $this->addFlash(
                'success',
                'Add to favourite successfully.'
            );

            return $this->redirect('/home');
        }else{
            return $this->redirect('/login');
        }
    }

    /**
     * @Route("/my_favorites", name="my_favorites")
     */
    public function myFavorite(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if(is_object($user)){
            $em = $this->getDoctrine()->getManager();
            $favourites = $em->getRepository('App\Entity\Favourite')
                ->findBy(array('user_id' => $user->getId() ));

            return $this->render('home/favourites.html.twig', [
                'id' => $request->query->get('id'),
                'favourites' => $favourites,
            ]);
        }else{
            return $this->redirect('/login');
        }
    }

    /**
     * @Route("/", name="homepage")
     * @Route("/blog/index", name="blog_index")
     */
    public function indexAction() 
    {
        return $this->redirect('/home');
    }
}
