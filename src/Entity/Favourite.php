<?php

namespace App\Entity;

use App\Repository\FavouriteRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FavouriteRepository::class)
 */
class Favourite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $user_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rover_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $camera_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $earth_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="integer")
     */
    private $api_id;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $api_image_src;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getRoverName(): ?string
    {
        return $this->rover_name;
    }

    public function setRoverName(string $rover_name): self
    {
        $this->rover_name = $rover_name;

        return $this;
    }

    public function getCameraName(): ?string
    {
        return $this->camera_name;
    }

    public function setCameraName(string $camera_name): self
    {
        $this->camera_name = $camera_name;

        return $this;
    }

    public function getEarthDate(): ?string
    {
        return $this->earth_date;
    }

    public function setEarthDate(string $earth_date): self
    {
        $this->earth_date = $earth_date;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getApiId(): ?int
    {
        return $this->api_id;
    }

    public function setApiId(int $api_id): self
    {
        $this->api_id = $api_id;

        return $this;
    }

    public function getApiImageSrc(): ?string
    {
        return $this->api_image_src;
    }

    public function setApiImageSrc(string $api_image_src): self
    {
        $this->api_image_src = $api_image_src;

        return $this;
    }
}
